#!/bin/bash

declare ACTION=""
declare MODE=""
declare COMPOSE_FILE_PATH=""
declare UTILS_PATH=""
declare STACK="hapi-fhir"

function init_vars() {
  ACTION=$1
  MODE=$2

  COMPOSE_FILE_PATH=$(
    cd "$(dirname "${BASH_SOURCE[0]}")" || exit
    pwd -P
  )

  UTILS_PATH="${COMPOSE_FILE_PATH}/../utils"

  readonly ACTION
  readonly MODE
  readonly COMPOSE_FILE_PATH
  readonly UTILS_PATH
  readonly STACK
}

# shellcheck disable=SC1091
function import_sources() {
  source "${UTILS_PATH}/docker-utils.sh"
  source "${UTILS_PATH}/config-utils.sh"
  source "${UTILS_PATH}/log.sh"
}

function restart_hapi_fhir() {
  local -r stackname="${FHIR_STACKNAME:-hapi-fhir}"
  if docker service ps -q ${stackname}_hapi-fhir &>/dev/null; then
    log info "Restarting HAPI FHIR.."
    try \
      "docker service update ${stackname}_hapi-fhir --env-add=hapi.fhir.ips_enabled=true --env-add=hapi.fhir.implementationguides.ips_1_0_0.url=https://hl7.org/fhir/uv/ips/package.tgz  --env-add=hapi.fhir.implementationguides.ips_1_0_0.name=hl7.fhir.uv.ips --env-add=hapi.fhir.implementationguides.ips_1_0_0.version=1.0.0" \
      throw \
      "Error setting hapi-fhir IG and IPS modules"
  else
    log warn "Service 'hapi-fhir' does not appear to be running... Skipping the restart of hapi-fhir"
  fi
}

function import_ig_via_openhim() {
  curl -k -X POST -H 'Content-Type: application/json' -d '{ "name": "IPS on Platform", "url": "https://jembi.github.io/TechHUB-IPS/" }' https://localhost:5000/ig
}

function initialize_package() {
  local hapi_fhir_dev_compose_filename=""

  if [ "${MODE}" == "dev" ]; then
    log info "Running package in DEV mode"
    hapi_fhir_dev_compose_filename="docker-compose.dev.yml"
  else
    log info "Running package in PROD mode"
  fi

  if [ "${CLUSTERED_MODE}" == "true" ]; then
    postgres_cluster_compose_filename="docker-compose-postgres.cluster.yml"
  fi

  (
    docker::await_service_status "postgres" "postgres-1" "Running"

    if [[ "${ACTION}" == "init" ]]; then
      docker::deploy_config_importer "postgres" "$COMPOSE_FILE_PATH/importer/docker-compose.config.yml" "hapi_db_config" "hapi-fhir"
    fi

    docker::deploy_service "$STACK" "${COMPOSE_FILE_PATH}" "docker-compose.yml" "$hapi_fhir_dev_compose_filename"

    restart_hapi_fhir
  ) ||
    {
      log error "Failed to deploy package"
      exit 1
    }
}

function destroy_package() {
  docker::stack_destroy "$STACK"

  if [[ "${CLUSTERED_MODE}" == "true" ]]; then
    log warn "Volumes are only deleted on the host on which the command is run. Postgres volumes on other nodes are not deleted"
  fi

  docker::prune_configs "hapi-fhir"
}

main() {
  init_vars "$@"
  import_sources

  if [[ "${ACTION}" == "init" ]] || [[ "${ACTION}" == "up" ]]; then
    if [[ "${CLUSTERED_MODE}" == "true" ]]; then
      log info "Running package in Cluster node mode"
    else
      log info "Running package in Single node mode"
    fi

    if [[ "${IMPORT_IG}" == "true" ]]; then
      log info "Importing FHIR IG"
      import_ig_via_openhim
      restart_hapi_fhir
    else
      initialize_package
    fi
    
  elif [[ "${ACTION}" == "down" ]]; then
    log info "Scaling down package"

    docker::scale_services "$STACK" 0
  elif [[ "${ACTION}" == "destroy" ]]; then
    log info "Destroying package"
    destroy_package
  else
    log error "Valid options are: init, up, down, or destroy"
  fi
}

main "$@"