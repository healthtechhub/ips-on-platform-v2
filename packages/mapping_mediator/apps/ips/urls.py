from django.urls import path, re_path
from apps.ips.view import RegisterPatientIDGeneratorView, FindAndRegisterPatientView, TransformAndForwardFhirData, ForwardFHIRPatientDetailsView

urlpatterns = [
    path('fhir', TransformAndForwardFhirData.as_view(), name='forward-fhir'),
    # Add the new URL pattern for forwarding FHIR Patient details using a class-based view
    # re_path(r'^fhir/Patient/?[^/]*$', ForwardFHIRPatientDetailsView.as_view(), name='forward_fhir_patient'),
    re_path(r'^fhir/Patient\??[^/]*$', ForwardFHIRPatientDetailsView.as_view(), name='forward_fhir_patient'),
    re_path(r'^fhir/links.*$', ForwardFHIRPatientDetailsView.as_view(), name='forward_fhir_links'),
    re_path(r'^fhir/(?P<extra>.*)$', TransformAndForwardFhirData.as_view(), name='forward-fhir-wildcard'),
    path('register-patient-id-generator', RegisterPatientIDGeneratorView.as_view(), name='register-patient-id-generator'),
    path('find-register-patient', FindAndRegisterPatientView.as_view(),
         name='register-patient-id-generator'),

]
