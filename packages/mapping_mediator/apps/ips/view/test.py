from .transformations import transform_fhir_bundle
from pprint import pprint

fhir_bundle = {
    "resourceType": "Bundle",
    "id": "{{$randomUUID}}",
    "meta": {
        "profile": [
            "http://fhir.health.gov.lk/ips/StructureDefinition/hhims-transactional-bundle"
        ]
    },
    "identifier": {
        "system": "http://fhir.health.gov.lk/ips/identifier/hhims-transactional",
        "value": "{{$randomUUID}}"
    },
    "type": "transaction",
    "timestamp": "{{timestamp}}",
    "entry": [
        {
            "fullUrl": "http://hapi-fhir:8080/Patient/{{patient}}",
            "resource": {
                "resourceType": "Patient",
                "id": "{{patient}}",
                "meta": {
                    "profile": [
                        "http://fhir.health.gov.lk/ips/StructureDefinition/hhims-patient"
                    ]
                },
                "identifier": [
                    {
                        "type": {
                            "coding": [
                                {
                                    "system": "http://fhir.health.gov.lk/ips/CodeSystem/cs-identifier-types",
                                    "code": "PHN"
                                }
                            ],
                            "text": "Personal Health Number"
                        },
                        "system": "http://fhir.health.gov.lk/ips/identifier/phn",
                        "value": "{{PHN}}"
                    },
                    {
                        "type": {
                            "coding": [
                                {
                                    "system": "http://fhir.health.gov.lk/ips/CodeSystem/cs-identifier-types",
                                    "code": "NIC"
                                }
                            ],
                            "text": "National identity number"
                        },
                        "system": "http://fhir.health.gov.lk/ips/identifier/nic",
                        "value": "{{NIC}}"
                    },
                    {
                        "type": {
                            "coding": [
                                {
                                    "system": "http://fhir.health.gov.lk/ips/CodeSystem/cs-identifier-types",
                                    "code": "NIC"
                                }
                            ],
                            "text": "National identity number"
                        },
                        "system": "http://fhir.health.gov.lk/ips/identifier/nic",
                        "value": "{{NIC}}"
                    }
                ],
                "name": [
                    {
                        "use": "official",
                        "family": "{{$randomLastName}}",
                        "given": [
                            "{{$randomNamePrefix}}",
                            "{{$randomFirstName}}",
                            "{{$randomFirstName}}"
                        ]
                    }
                ],
                "telecom": [
                    {
                        "system": "phone",
                        "value": "+{{$randomPhoneNumber}}"
                    },
                    {
                        "system": "email",
                        "value": "{{$randomEmail}}"
                    },
                    {
                        "system": "email",
                        "value": "{{$randomEmail}}"
                    }
                ],
                "gender": "{{gender}}",
                "birthDate": "{{birthdate}}",
                "address": [
                    {
                        "type": "postal",
                        "line": [
                            "177",
                            "Nawala Road"
                        ],
                        "city": "Nugegoda",
                        "district": "Ampara",
                        "state": "Colombo",
                        "postalCode": "32350",
                        "country": "LK"
                    }
                ]
            },
            "request": {
                "method": "PUT",
                "url": "Patient/{{patient}}"
            }
        },
        {
            "fullUrl": "http://hapi-fhir:8080/Encounter/{{encounter}}",
            "resource": {
                "resourceType": "Encounter",
                "id": "{{encounter}}",
                "meta": {
                    "profile": [
                        "http://fhir.health.gov.lk/ips/StructureDefinition/hhims-target-facility-encounter"
                    ]
                },
                "status": "finished",
                "class": {
                    "system": "http://terminology.hl7.org/CodeSystem/v3-ActCode",
                    "code": "AMB"
                },
                "subject": {
                    "reference": "Patient/{{patient}}"
                },
                "participant": [
                    {
                        "individual": {
                            "reference": "Practitioner/{{practitioner}}"
                        }
                    }
                ],
                "period": {
                    "start": "{{encounterDate}}",
                    "end": "{{encounterDate}}"
                },
                "reasonCode": [
                    {
                        "coding": [
                            {
                                "system": "http://snomed.info/sct",
                                "code": "140004"
                            }
                        ]
                    }
                ],
                "location": [
                    {
                        "location": {
                            "reference": "Location/{{location}}"
                        }
                    }
                ]
            },
            "request": {
                "method": "PUT",
                "url": "Encounter/{{encounter}}"
            }
        },
        {
            "fullUrl": "http://hapi-fhir:8080/Condition/{{condition}}",
            "resource": {
                "resourceType": "Condition",
                "id": "{{condition}}",
                "meta": {
                    "profile": [
                        "http://fhir.health.gov.lk/ips/StructureDefinition/medical-history-hhims"
                    ]
                },
                "clinicalStatus": {
                    "coding": [
                        {
                            "system": "http://terminology.hl7.org/CodeSystem/condition-clinical",
                            "code": "inactive"
                        }
                    ]
                },
                "code": {
                    "coding": [
                        {
                            "system": "http://hl7.org/fhir/uv/ips/CodeSystem/absent-unknown-uv-ips",
                            "code": "no-problem-info"
                        }
                    ]
                },
                "subject": {
                    "reference": "Patient/{{patient}}"
                },
                "encounter": {
                    "reference": "Encounter/{{encounter}}"
                },
                "recordedDate": "{{conditionRecordedDate}}"
            },
            "request": {
                "method": "PUT",
                "url": "Condition/{{condition}}"
            }
        },
        {
            "fullUrl": "http://hapi-fhir:8080/Communication/{{communication}}",
            "resource": {
                "resourceType": "Communication",
                "id": "{{communication}}",
                "meta": {
                    "profile": [
                        "http://fhir.health.gov.lk/ips/StructureDefinition/notifiable-diseases-notified"
                    ]
                },
                "status": "completed",
                "subject": {
                    "reference": "Patient/{{patient}}"
                },
                "encounter": {
                    "reference": "Encounter/{{encounter}}"
                },
                "sent": "{{communicationSent}}"
            },
            "request": {
                "method": "PUT",
                "url": "Communication/{{communication}}"
            }
        },
        {
            "fullUrl": "http://hapi-fhir:8080/Observation/{{obs-weight}}",
            "resource": {
                "resourceType": "Observation",
                "id": "{{obs-weight}}",
                "meta": {
                    "profile": [
                        "http://fhir.health.gov.lk/ips/StructureDefinition/hhims-weight"
                    ]
                },
                "status": "final",
                "category": [
                    {
                        "coding": [
                            {
                                "system": "http://terminology.hl7.org/CodeSystem/observation-category",
                                "code": "vital-signs"
                            }
                        ]
                    }
                ],
                "code": {
                    "coding": [
                        {
                            "system": "http://loinc.org",
                            "code": "29463-7"
                        }
                    ],
                    "text": "Body Weight"
                },
                "subject": {
                    "reference": "Patient/{{patient}}"
                },
                "encounter": {
                    "reference": "Encounter/{{encounter}}"
                },
                "effectiveDateTime": "{{obs-weight-date}}",
                "performer": [
                    {
                        "reference": "Organization/{{organization}}"
                    },
                    {
                        "reference": "Practitioner/{{practitioner}}"
                    }
                ],
                "valueQuantity": {
                    "value": 110,
                    "unit": "kg",
                    "system": "http://unitsofmeasure.org",
                    "code": "kg"
                }
            },
            "request": {
                "method": "PUT",
                "url": "Observation/{{obs-weight}}"
            }
        },
        {
            "fullUrl": "http://hapi-fhir:8080/AllergyIntolerance/{{allergies}}",
            "resource": {
                "resourceType": "AllergyIntolerance",
                "id": "{{allergies}}",
                "meta": {
                    "profile": [
                        "http://fhir.health.gov.lk/ips/StructureDefinition/hhims-allergy-intolerance"
                    ]
                },
                "clinicalStatus": {
                    "coding": [
                        {
                            "system": "http://terminology.hl7.org/CodeSystem/allergyintolerance-clinical",
                            "code": "active"
                        }
                    ]
                },
                "verificationStatus": {
                    "coding": [
                        {
                            "system": "http://terminology.hl7.org/CodeSystem/allergyintolerance-verification",
                            "code": "confirmed"
                        }
                    ]
                },
                "type": "allergy",
                "code": {
                    "coding": [
                        {
                            "system": "http://snomed.info/sct",
                            "code": "414285001"
                        }
                    ],
                    "text": "Allergy to food"
                },
                "patient": {
                    "reference": "Patient/{{patient}}"
                },
                "encounter": {
                    "reference": "Encounter/{{encounter}}"
                },
                "onsetDateTime": "{{allergy-onset-date}}",
                "recorder": {
                    "reference": "Practitioner/{{practitioner}}"
                }
            },
            "request": {
                "method": "PUT",
                "url": "AllergyIntolerance/{{allergies}}"
            }
        },
        {
            "fullUrl": "http://hapi-fhir:8080/MedicationRequest/{{medicationrequest}}",
            "resource": {
                "resourceType": "MedicationRequest",
                "id": "{{medicationrequest}}",
                "meta": {
                    "profile": [
                        "http://fhir.health.gov.lk/ips/StructureDefinition/medication-request"
                    ]
                },
                "identifier": [
                    {
                        "type": {
                            "coding": [
                                {
                                    "system": "http://terminology.hl7.org/CodeSystem/v2-0203",
                                    "code": "FILL",
                                    "display": "Filler Identifier"
                                }
                            ],
                            "text": "Prescription identifier"
                        },
                        "system": "http://fhir.health.gov.lk/ips/identifier/prescription",
                        "value": "{{prescriptionId}}"
                    }
                ],
                "status": "completed",
                "intent": "order",
                "medicationCodeableConcept": {
                    "coding": [
                        {
                            "system": "http://fhir.health.gov.lk/ips/CodeSystem/cs-medication-name",
                            "code": "Vitamin-A-&-D"
                        }
                    ],
                    "text": "Medication"
                },
                "subject": {
                    "reference": "Patient/{{patient}}"
                },
                "encounter": {
                    "reference": "Encounter/{{encounter}}"
                },
                "authoredOn": "{{medication-request-authored-on}}",
                "requester": {
                    "reference": "Practitioner/{{practitioner}}"
                },
                "performer": {
                    "reference": "Practitioner/{{practitioner}}"
                },
                "recorder": {
                    "reference": "Practitioner/{{practitioner}}"
                },
                "note": [
                    {
                        "authorReference": {
                            "reference": "Organization/{{organization}}"
                        },
                        "time": "2023-10-11T17:21:33-08:00",
                        "text": "Additional information regarding the patient's medication prescription"
                    }
                ],
                "dosageInstruction": [
                    {
                        "timing": {
                            "repeat": {
                                "count": 2,
                                "duration": 2,
                                "durationUnit": "d",
                                "frequency": 2,
                                "period": 3,
                                "periodUnit": "d"
                            },
                            "code": {
                                "coding": [
                                    {
                                        "system": "http://terminology.hl7.org/CodeSystem/v3-GTSAbbreviation",
                                        "code": "BID"
                                    }
                                ]
                            }
                        },
                        "doseAndRate": [
                            {
                                "doseQuantity": {
                                    "value": 3
                                }
                            }
                        ]
                    }
                ],
                "dispenseRequest": {
                    "quantity": {
                        "value": 5
                    }
                }
            },
            "request": {
                "method": "PUT",
                "url": "MedicationRequest/{{medicationrequest}}"
            }
        },
        {
            "fullUrl": "http://hapi-fhir:8080/MedicationDispense/{{medicationdispense}}",
            "resource": {
                "resourceType": "MedicationDispense",
                "id": "{{medicationdispense}}",
                "meta": {
                    "profile": [
                        "http://fhir.health.gov.lk/ips/StructureDefinition/drug-dispensation"
                    ]
                },
                "status": "completed",
                "medicationCodeableConcept": {
                    "coding": [
                        {
                            "system": "http://fhir.health.gov.lk/ips/CodeSystem/cs-medication-name",
                            "code": "Vitamin-A-&-D"
                        }
                    ],
                    "text": "Medication"
                },
                "subject": {
                    "reference": "Patient/{{patient}}"
                },
                "context": {
                    "reference": "Encounter/{{encounter}}"
                },
                "performer": [
                    {
                        "actor": {
                            "reference": "Practitioner/{{practitioner}}"
                        }
                    }
                ],
                "location": {
                    "reference": "Location/{{location}}"
                },
                "authorizingPrescription": [
                    {
                        "reference": "MedicationRequest/{{medicationrequest}}"
                    }
                ],
                "whenHandedOver": "{{whenHandedOver}}",
                "receiver": [
                    {
                        "reference": "Patient/{{patient}}"
                    }
                ]
            },
            "request": {
                "method": "PUT",
                "url": "MedicationDispense/{{medicationdispense}}"
            }
        },
        {
            "fullUrl": "http://hapi-fhir:8080/MedicationAdministration/{{medicationadministration}}",
            "resource": {
                "resourceType": "MedicationAdministration",
                "id": "{{medicationadministration}}",
                "meta": {
                    "profile": [
                        "http://fhir.health.gov.lk/ips/StructureDefinition/injection"
                    ]
                },
                "status": "completed",
                "medicationCodeableConcept": {
                    "coding": [
                        {
                            "system": "http://snomed.info/sct",
                            "code": "2571007"
                        }
                    ],
                    "text": "Busulfan"
                },
                "subject": {
                    "reference": "Patient/{{patient}}"
                },
                "context": {
                    "reference": "Encounter/{{encounter}}"
                },
                "effectiveDateTime": "{{medicationadministration-date}}",
                "performer": [
                    {
                        "actor": {
                            "reference": "Practitioner/{{practitioner}}"
                        }
                    }
                ],
                "dosage": {
                    "route": {
                        "coding": [
                            {
                                "system": "http://snomed.info/sct",
                                "code": "47625008"
                            }
                        ],
                        "text": "Intravenous route"
                    },
                    "dose": {
                        "value": 12
                    }
                }
            },
            "request": {
                "method": "PUT",
                "url": "MedicationAdministration/{{medicationadministration}}"
            }
        },
        {
            "fullUrl": "http://hapi-fhir:8080/Procedure/{{procedure}}",
            "resource": {
                "resourceType": "Procedure",
                "id": "{{procedure}}",
                "meta": {
                    "profile": [
                        "http://fhir.health.gov.lk/ips/StructureDefinition/procedure"
                    ]
                },
                "status": "in-progress",
                "category": {
                    "coding": [
                        {
                            "system": "http://snomed.info/sct",
                            "code": "409073007"
                        }
                    ]
                },
                "code": {
                    "coding": [
                        {
                            "system": "http://fhir.health.gov.lk/ips/CodeSystem/cs-procedure-imaging",
                            "code": "X-Ray"
                        }
                    ]
                },
                "subject": {
                    "reference": "Patient/{{patient}}"
                },
                "encounter": {
                    "reference": "Encounter/{{encounter}}"
                },
                "performedDateTime": "{{performedDateTime}}",
                "recorder": {
                    "reference": "Practitioner/{{practitioner}}"
                },
                "asserter": {
                    "reference": "Practitioner/{{practitioner}}"
                },
                "performer": [
                    {
                        "actor": {
                            "reference": "Practitioner/{{practitioner}}"
                        }
                    }
                ],
                "location": {
                    "reference": "Location/{{location}}"
                }
            },
            "request": {
                "method": "PUT",
                "url": "Procedure/{{procedure}}"
            }
        },
        {
            "fullUrl": "http://hapi-fhir:8080/ImagingStudy/{{imagingstudy}}",
            "resource": {
                "resourceType": "ImagingStudy",
                "id": "{{imagingstudy}}",
                "meta": {
                    "profile": [
                        "http://fhir.health.gov.lk/ips/StructureDefinition/imaging-study"
                    ]
                },
                "status": "registered",
                "subject": {
                    "reference": "Patient/{{patient}}"
                },
                "encounter": {
                    "reference": "Encounter/{{encounter}}"
                },
                "started": "{{imagingstudy-started}}",
                "basedOn": [
                    {
                        "reference": "ServiceRequest/{{imagingservicerequest}}"
                    }
                ],
                "referrer": {
                    "reference": "Practitioner/{{practitioner}}"
                },
                "procedureReference": {
                    "reference": "Procedure/{{procedure}}"
                },
                "location": {
                    "reference": "Location/{{location}}"
                },
                "description": "Imaging Description",
                "series": [
                    {
                        "uid": "89c0c298-6c30-11ee-b962-0242ac120002",
                        "modality": {
                            "system": "http://dicom.nema.org/resources/ontology/DCM",
                            "code": "XA"
                        },
                        "performer": [
                            {
                                "actor": {
                                    "reference": "Practitioner/{{practitioner}}"
                                }
                            }
                        ]
                    }
                ]
            },
            "request": {
                "method": "PUT",
                "url": "ImagingStudy/{{imagingstudy}}"
            }
        },
        {
            "fullUrl": "http://hapi-fhir:8080/ServiceRequest/{{generalreferralservicerequest}}",
            "resource": {
                "resourceType": "ServiceRequest",
                "id": "{{generalreferralservicerequest}}",
                "meta": {
                    "profile": [
                        "http://fhir.health.gov.lk/ips/StructureDefinition/general-referral-request"
                    ]
                },
                "status": "completed",
                "intent": "order",
                "code": {
                    "coding": [
                        {
                            "system": "http://snomed.info/sct",
                            "code": "3457005"
                        }
                    ],
                    "text": "Patient referral"
                },
                "subject": {
                    "reference": "Patient/{{patient}}"
                },
                "encounter": {
                    "reference": "Encounter/{{encounter}}"
                },
                "occurrenceDateTime": "{{generalreferralservicerequest-date}}",
                "requester": {
                    "reference": "Practitioner/{{practitioner}}"
                },
                "locationReference": [
                    {
                        "reference": "Location/{{location}}"
                    }
                ]
            },
            "request": {
                "method": "PUT",
                "url": "ServiceRequest/{{generalreferralservicerequest}}"
            }
        },
        {
            "fullUrl": "http://hapi-fhir:8080/ServiceRequest/{{investigationsservicerequest}}",
            "resource": {
                "resourceType": "ServiceRequest",
                "id": "{{investigationsservicerequest}}",
                "meta": {
                    "profile": [
                        "http://fhir.health.gov.lk/ips/StructureDefinition/investigations-request"
                    ]
                },
                "status": "completed",
                "intent": "order",
                "code": {
                    "coding": [
                        {
                            "system": "http://fhir.health.gov.lk/ips/CodeSystem/cs-investigation",
                            "code": "FBC-WBC-th/uL"
                        }
                    ],
                    "text": "Investigation requested"
                },
                "subject": {
                    "reference": "Patient/{{patient}}"
                },
                "encounter": {
                    "reference": "Encounter/{{encounter}}"
                },
                "occurrenceDateTime": "{{investigationsservicerequest-date}}",
                "requester": {
                    "reference": "Practitioner/{{practitioner}}"
                },
                "locationReference": [
                    {
                        "reference": "Location/{{location}}"
                    }
                ]
            },
            "request": {
                "method": "PUT",
                "url": "ServiceRequest/{{investigationsservicerequest}}"
            }
        },
        {
            "fullUrl": "http://hapi-fhir:8080/ServiceRequest/{{imagingservicerequest}}",
            "resource": {
                "resourceType": "ServiceRequest",
                "id": "{{imagingservicerequest}}",
                "meta": {
                    "profile": [
                        "http://fhir.health.gov.lk/ips/StructureDefinition/imaging-request"
                    ]
                },
                "status": "completed",
                "intent": "order",
                "code": {
                    "coding": [
                        {
                            "system": "http://fhir.health.gov.lk/ips/CodeSystem/cs-procedure-imaging",
                            "code": "X-Ray"
                        }
                    ],
                    "text": "Imaging requested"
                },
                "subject": {
                    "reference": "Patient/{{patient}}"
                },
                "encounter": {
                    "reference": "Encounter/{{encounter}}"
                },
                "occurrenceDateTime": "{{imagingservicerequest-date}}",
                "requester": {
                    "reference": "Practitioner/{{practitioner}}"
                },
                "locationReference": [
                    {
                        "reference": "Location/{{location}}"
                    }
                ]
            },
            "request": {
                "method": "PUT",
                "url": "ServiceRequest/{{imagingservicerequest}}"
            }
        },
        {
            "fullUrl": "http://hapi-fhir:8080/Task/{{hhimsreferraltask}}",
            "resource": {
                "resourceType": "Task",
                "id": "{{hhimsreferraltask}}",
                "meta": {
                    "profile": [
                        "http://fhir.health.gov.lk/ips/StructureDefinition/referral-task"
                    ]
                },
                "status": "completed",
                "intent": "order",
                "priority": "routine",
                "description": "some information regarding the need for the referral request, if any.",
                "focus": {
                    "reference": "ServiceRequest/{{generalreferralservicerequest}}"
                },
                "for": {
                    "reference": "Patient/{{patient}}"
                },
                "encounter": {
                    "reference": "Encounter/{{encounter}}"
                },
                "authoredOn": "{{hhimsreferraltask-date}}",
                "requester": {
                    "reference": "Practitioner/{{practitioner}}"
                },
                "location": {
                    "reference": "Location/{{location}}"
                }
            },
            "request": {
                "method": "PUT",
                "url": "Task/{{hhimsreferraltask}}"
            }
        },
        {
            "fullUrl": "http://hapi-fhir:8080/Task/{{investigationstask}}",
            "resource": {
                "resourceType": "Task",
                "id": "{{investigationstask}}",
                "meta": {
                    "profile": [
                        "http://fhir.health.gov.lk/ips/StructureDefinition/investigations-task"
                    ]
                },
                "basedOn": [
                    {
                        "reference": "ServiceRequest/{{investigationsservicerequest}}"
                    }
                ],
                "status": "completed",
                "intent": "order",
                "priority": "routine",
                "description": "some information regarding the need for the investigations request, if any.",
                "for": {
                    "reference": "Patient/{{patient}}"
                },
                "encounter": {
                    "reference": "Encounter/{{encounter}}"
                },
                "executionPeriod": {
                    "start": "{{investigationstask-date}}",
                    "end": "{{investigationstask-date}}"
                },
                "authoredOn": "{{investigationstask-authoredOn}}",
                "requester": {
                    "reference": "Practitioner/{{practitioner}}"
                },
                "location": {
                    "reference": "Location/{{location}}"
                }
            },
            "request": {
                "method": "PUT",
                "url": "Task/{{investigationstask}}"
            }
        },
        {
            "fullUrl": "http://hapi-fhir:8080/Task/{{imagingtask}}",
            "resource": {
                "resourceType": "Task",
                "id": "{{imagingtask}}",
                "meta": {
                    "profile": [
                        "http://fhir.health.gov.lk/ips/StructureDefinition/imaging-task"
                    ]
                },
                "basedOn": [
                    {
                        "reference": "ServiceRequest/{{imagingservicerequest}}"
                    }
                ],
                "status": "completed",
                "intent": "order",
                "priority": "routine",
                "description": "some information regarding the need for the imaging request, if any.",
                "for": {
                    "reference": "Patient/{{patient}}"
                },
                "encounter": {
                    "reference": "Encounter/{{encounter}}"
                },
                "executionPeriod": {
                    "start": "{{imagingtask-date}}",
                    "end": "{{imagingtask-date}}"
                },
                "authoredOn": "{{imagingtask-authoredOn}}",
                "requester": {
                    "reference": "Practitioner/{{practitioner}}"
                },
                "location": {
                    "reference": "Location/{{location}}"
                }
            },
            "request": {
                "method": "PUT",
                "url": "Task/{{imagingtask}}"
            }
        },
        {
            "fullUrl": "http://hapi-fhir:8080/Practitioner/{{practitioner}}",
            "resource": {
                "resourceType": "Practitioner",
                "id": "{{practitioner}}",
                "meta": {
                    "profile": [
                        "http://fhir.health.gov.lk/ips/StructureDefinition/practitioner"
                    ]
                },
                "name": [
                    {
                        "family": "{{$randomLastName}}",
                        "given": [
                            "{{$randomFirstName}}",
                            "{{$randomFirstName}}"
                        ],
                        "prefix": [
                            "Dr"
                        ]
                    }
                ],
                "telecom": [
                    {
                        "system": "phone",
                        "value": "{{$randomPhoneNumber}}",
                        "use": "work"
                    },
                    {
                        "system": "email",
                        "value": "{{$randomEmail}}",
                        "use": "home"
                    }
                ]
            },
            "request": {
                "method": "PUT",
                "url": "Practitioner/{{practitioner}}"
            }
        },
        {
            "fullUrl": "http://hapi-fhir:8080/Location/{{location}}",
            "resource": {
                "resourceType": "Location",
                "id": "{{location}}",
                "meta": {
                    "profile": [
                        "http://fhir.health.gov.lk/ips/StructureDefinition/providers-location"
                    ]
                },
                "identifier": [
                    {
                        "type": {
                            "coding": [
                                {
                                    "system": "http://fhir.health.gov.lk/ips/CodeSystem/cs-identifier-types",
                                    "code": "PLOC"
                                }
                            ],
                            "text": "Provider location identifier"
                        },
                        "system": "http://fhir.health.gov.lk/ips/identifier/provider-location",
                        "value": "{{LOCID}}"
                    },
                    {
                        "type": {
                            "coding": [
                                {
                                    "system": "http://fhir.health.gov.lk/ips/CodeSystem/cs-identifier-types",
                                    "code": "PLOC"
                                }
                            ],
                            "text": "Provider location identifier"
                        },
                        "system": "http://fhir.health.gov.lk/ips/identifier/provider-location",
                        "value": "{{LOCID}}"
                    }
                ],
                "status": "active",
                "name": "Name for the location",
                "telecom": [
                    {
                        "system": "phone",
                        "value": "+{{$randomPhoneNumber}}"
                    },
                    {
                        "system": "email",
                        "value": "{{$randomEmail}}"
                    },
                    {
                        "system": "email",
                        "value": "{{$randomEmail}}"
                    }
                ],
                "address": {
                    "type": "postal",
                    "line": [
                        "177",
                        "Nawala Road"
                    ],
                    "city": "Nugegoda",
                    "district": "Ampara",
                    "state": "Colombo",
                    "postalCode": "32350",
                    "country": "LK"
                },
                "managingOrganization": {
                    "reference": "Organization/{{organization}}"
                }
            },
            "request": {
                "method": "PUT",
                "url": "Location/{{location}}"
            }
        },
        {
            "fullUrl": "http://hapi-fhir:8080/Organization/{{organization}}",
            "resource": {
                "resourceType": "Organization",
                "id": "{{organization}}",
                "meta": {
                    "profile": [
                        "http://fhir.health.gov.lk/ips/StructureDefinition/organization"
                    ]
                },
                "identifier": [
                    {
                        "type": {
                            "coding": [
                                {
                                    "system": "http://terminology.hl7.org/CodeSystem/v2-0203",
                                    "code": "XX"
                                }
                            ],
                            "text": "Organization identifier"
                        },
                        "system": "http://fhir.health.gov.lk/ips/identifier/organization",
                        "value": "facility1"
                    }
                ],
                "name": "Some Health Facility"
            },
            "request": {
                "method": "PUT",
                "url": "Organization/{{organization}}"
            }
        }
    ]
}

pprint(transform_fhir_bundle(fhir_bundle))
