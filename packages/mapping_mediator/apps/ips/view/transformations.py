import json
import os
from pprint import pprint

def load_ocl_mappings_from_json():
    """Load OCL mappings from JSON files in the 'data' directory into a structured dictionary."""
    # Construct the path to the 'data' folder at the same level as this script
    # Path to the current script
    current_script_path = os.path.abspath(__file__)

    # Directory of the current script
    current_dir = os.path.dirname(current_script_path)
    directory_path = os.path.join(current_dir, 'data')
    data = {}
    
    # Ensure the 'data' directory exists
    if not os.path.exists(directory_path):
        print(f"The directory '{directory_path}' does not exist.")
        return data

    # Iterate over all JSON files in the 'data' directory
    for filename in os.listdir(directory_path):
        if filename.endswith('.json'):  # Ensure processing only JSON files
            file_path = os.path.join(directory_path, filename)
            with open(file_path, mode='r', encoding='utf-8') as file:
                mappings = json.load(file)
                # Process each mapping
                for mapping in mappings:
                    from_code = mapping['from_concept_code']
                    # Create a structured mapping for each from_concept_code
                    if from_code not in data:
                        data[from_code] = []
                    data[from_code].append({
                        'from_concept_code': mapping['from_concept_code'],
                        'to_concept_code': mapping['to_concept_code'],
                        'from_concept_name_resolved': mapping.get('from_concept_name_resolved', ''),
                        'to_concept_name_resolved': mapping.get('to_concept_name_resolved', ''),
                        'from_concept_url': mapping['from_concept_url'],
                        'to_concept_url': mapping['to_concept_url']
                    })
    return data

# No need to pass the directory_path as an argument
ocl_data = load_ocl_mappings_from_json()

def transform_coding(coding):
    """
    Transforms a coding object based on provided mappings. The system is constructed
    from a base URL and the to_concept_url from the mapping.

    Args:
        coding (dict): The original coding to transform.
        mappings (list): A list of mappings to apply, where each mapping is a dictionary.

    Returns:
        dict: The transformed coding if a mapping was found; otherwise, the original coding.
    """
    base_url = "https://app.openconceptlab.org/#"
    mapping = ocl_data.get(coding.get('code'))
    if mapping:
        # Construct the system URL using base_url and to_concept_url
        system_url = f"{base_url}{mapping[0].get('to_concept_url')}"
        return {
            'system': system_url,
            'code': mapping[0]['to_concept_code'],
            'display': mapping[0].get('to_concept_name_resolved', coding.get('display'))  # Keeping original display if not provided
        }
    # Return the original coding if no mapping is found
    return coding

def transform_resource_codings(resource, resource_type):
    """
    Recursively transforms codings within a FHIR resource.

    Args:
        resource (dict): The FHIR resource to transform.
        resource_type (str): The type of the FHIR resource.
    """
    if isinstance(resource, dict):
        for key, value in resource.items():
            if isinstance(value, list):
                # This could be a list of resources, codings, or other objects.
                # We iterate through the list and apply transformation recursively.
                for item in value:
                    if isinstance(item, dict) and 'coding' in item:
                        # If the item is a dictionary and has a 'coding' key, we transform the codings.
                        new_codings = [transform_coding(coding) for coding in item['coding']]
                        item['coding'] = new_codings
                    else:
                        # If the item is a dictionary but doesn't have a 'coding' key,
                        # it might be a nested structure that needs recursive transformation.
                        transform_resource_codings(item, resource_type)
            elif isinstance(value, dict):
                # If the value is a dictionary, it could be a nested resource or coding,
                # so we handle it recursively.
                transform_resource_codings(value, resource_type)
    elif isinstance(resource, list):
        # If the resource itself is a list at the call level, we apply transformation to each item.
        for item in resource:
            transform_resource_codings(item, resource_type)

def transform_fhir_bundle(fhir_bundle):
    """
    Transforms all resources within a FHIR bundle based on OCL mappings.

    Args:
        fhir_bundle (dict): The FHIR bundle containing resources to be transformed.
    """
    for entry in fhir_bundle.get('entry', []):
        resource = entry.get('resource')
        if resource:
            resource_type = resource.get('resourceType')
            transform_resource_codings(resource, resource_type)
    return fhir_bundle

# fhir_bundle = {
#     "resourceType": "Bundle",
#     "id": "16b4c214-dbf0-43a8-b37c-9a0e7b1ebad0",
#     "meta": {
#         "profile": [
#             "http://fhir.health.gov.lk/ips/StructureDefinition/hims-transactional-bundle"
#         ]
#     },
#     "identifier": {
#         "system": "http://fhir.health.gov.lk/ips/identifier/hims-transactional",
#         "value": "8e841967-d299-45ec-9f42-ac872d90ab75"
#     },
#     "type": "transaction",
#     "timestamp": "2024-02-04T12:26:09.172Z",
#     "entry": [
#         {
#             "fullUrl": "http://hapi-fhir:8080/fhir/Patient/d8aea1c5-bcc6-4edd-b3d7-f1c268ca63ab",
#             "resource": {
#                 "resourceType": "Patient",
#                 "id": "d8aea1c5-bcc6-4edd-b3d7-f1c268ca63ab",
#                 "meta": {
#                     "profile": [
#                         "http://fhir.health.gov.lk/ips/StructureDefinition/hims-patient"
#                     ]
#                 },
#                 "identifier": [
#                     {
#                         "type": {
#                             "coding": [
#                                 {
#                                     "system": "http://fhir.health.gov.lk/ips/CodeSystem/cs-identifier-types",
#                                     "code": "PHN"
#                                 }
#                             ],
#                             "text": "Personal Health Number"
#                         },
#                         "system": "http://fhir.health.gov.lk/ips/identifier/phn",
#                         "value": "338664"
#                     },
#                     {
#                         "type": {
#                             "coding": [
#                                 {
#                                     "system": "http://terminology.hl7.org/CodeSystem/v2-0203",
#                                     "code": "PPN"
#                                 }
#                             ],
#                             "text": "Passport number"
#                         },
#                         "system": "http://fhir.health.gov.lk/ips/identifier/passport",
#                         "value": "708933"
#                     },
#                     {
#                         "type": {
#                             "coding": [
#                                 {
#                                     "system": "http://terminology.hl7.org/CodeSystem/v2-0203",
#                                     "code": "PPN"
#                                 }
#                             ],
#                             "text": "Passport number"
#                         },
#                         "system": "http://fhir.health.gov.lk/ips/identifier/passport",
#                         "value": "708933"
#                     },
#                     {
#                         "type": {
#                             "coding": [
#                                 {
#                                     "system": "http://terminology.hl7.org/CodeSystem/v2-0203",
#                                     "code": "DL"
#                                 }
#                             ],
#                             "text": "Driver's license number"
#                         },
#                         "system": "http://fhir.health.gov.lk/ips/identifier/drivers",
#                         "value": "105261"
#                     },
#                     {
#                         "type": {
#                             "coding": [
#                                 {
#                                     "system": "http://terminology.hl7.org/CodeSystem/v2-0203",
#                                     "code": "DL"
#                                 }
#                             ],
#                             "text": "Driver's license number"
#                         },
#                         "system": "http://fhir.health.gov.lk/ips/identifier/drivers",
#                         "value": "105261"
#                     },
#                     {
#                         "type": {
#                             "coding": [
#                                 {
#                                     "system": "http://fhir.health.gov.lk/ips/CodeSystem/cs-identifier-types",
#                                     "code": "NIC"
#                                 }
#                             ],
#                             "text": "National identity number"
#                         },
#                         "system": "http://fhir.health.gov.lk/ips/identifier/nic",
#                         "value": "3789052008"
#                     },
#                     {
#                         "type": {
#                             "coding": [
#                                 {
#                                     "system": "http://fhir.health.gov.lk/ips/CodeSystem/cs-identifier-types",
#                                     "code": "NIC"
#                                 }
#                             ],
#                             "text": "National identity number"
#                         },
#                         "system": "http://fhir.health.gov.lk/ips/identifier/nic",
#                         "value": "3789052008"
#                     },
#                     {
#                         "type": {
#                             "coding": [
#                                 {
#                                     "system": "http://fhir.health.gov.lk/ips/CodeSystem/cs-identifier-types",
#                                     "code": "SCN"
#                                 }
#                             ],
#                             "text": "Senior Citizen Number"
#                         },
#                         "system": "http://fhir.health.gov.lk/ips/identifier/scn",
#                         "value": "1574667873"
#                     },
#                     {
#                         "type": {
#                             "coding": [
#                                 {
#                                     "system": "http://fhir.health.gov.lk/ips/CodeSystem/cs-identifier-types",
#                                     "code": "SCN"
#                                 }
#                             ],
#                             "text": "Senior Citizen Number"
#                         },
#                         "system": "http://fhir.health.gov.lk/ips/identifier/scn",
#                         "value": "1574667873"
#                     }
#                 ],
#                 "name": [
#                     {
#                         "use": "official",
#                         "family": "Green",
#                         "given": [
#                             "Raul",
#                             "Hilario"
#                         ],
#                         "prefix": [
#                             "Dr."
#                         ]
#                     }
#                 ],
#                 "telecom": [
#                     {
#                         "system": "phone",
#                         "value": "923-679-5647"
#                     },
#                     {
#                         "system": "email",
#                         "value": "Colt51@hotmail.com"
#                     },
#                     {
#                         "system": "email",
#                         "value": "Margie49@hotmail.com"
#                     }
#                 ],
#                 "gender": "unknown",
#                 "birthDate": "1950-02-04",
#                 "address": [
#                     {
#                         "type": "postal",
#                         "line": [
#                             "177",
#                             "Nawala Road"
#                         ],
#                         "city": "Nugegoda",
#                         "district": "Ampara",
#                         "state": "Colombo",
#                         "postalCode": "32350",
#                         "country": "LK"
#                     }
#                 ]
#             },
#             "request": {
#                 "method": "PUT",
#                 "url": "Patient/d8aea1c5-bcc6-4edd-b3d7-f1c268ca63ab"
#             }
#         },
#         {
#             "fullUrl": "http://hapi-fhir:8080/fhir/Encounter/0c0e4309-7cde-4766-9eaa-cdf5de6d44fb",
#             "resource": {
#                 "resourceType": "Encounter",
#                 "id": "0c0e4309-7cde-4766-9eaa-cdf5de6d44fb",
#                 "meta": {
#                     "profile": [
#                         "http://fhir.health.gov.lk/ips/StructureDefinition/hims-target-facility-encounter"
#                     ]
#                 },
#                 "status": "finished",
#                 "class": {
#                     "system": "http://terminology.hl7.org/CodeSystem/v3-ActCode",
#                     "code": "AMB"
#                 },
#                 "subject": {
#                     "reference": "Patient/d8aea1c5-bcc6-4edd-b3d7-f1c268ca63ab"
#                 },
#                 "participant": [
#                     {
#                         "individual": {
#                             "reference": "Practitioner/a715e5ef-7b4d-4758-b732-78b223a18e79"
#                         }
#                     }
#                 ],
#                 "period": {
#                     "start": "2024-02-04T12:26:09.172Z",
#                     "end": "2024-02-04T12:26:09.172Z"
#                 },
#                 "reasonCode": [
#                     {
#                         "coding": [
#                             {
#                                 "system": "https://api.openconceptlab.org/orgs/mohlk/sources/LKNehrOpdRFECodes/concepts/LKRFE65/names/17629029/",
#                                 "code": "LKRFE65"
#                             }
#                         ]
#                     }
#                 ],
#                 "location": [
#                     {
#                         "location": {
#                             "reference": "Location/63b8722e-eee0-4a23-a245-1d57ba2cf286"
#                         }
#                     }
#                 ]
#             },
#             "request": {
#                 "method": "PUT",
#                 "url": "Encounter/0c0e4309-7cde-4766-9eaa-cdf5de6d44fb"
#             }
#         }


#     ]
# }

# pprint(transform_fhir_bundle(fhir_bundle))