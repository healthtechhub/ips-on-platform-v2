import datetime
import pytz
import copy
import json
import requests
import uuid
import environ
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods
from rest_framework.views import APIView
from rest_framework.response import Response
from typing import Dict, Any
from rest_framework.permissions import AllowAny
from django.http import HttpResponse

# Initialize environment variables
env = environ.Env(DEBUG=(bool, False))
openhim_host = env('OPENHIM_URL')
openhim_port = env('OPENHIM_PORT')

from .transformations import transform_fhir_bundle

class TransformAndForwardFhirData(APIView):
    permission_classes = [AllowAny]

    def get(self, request, extra=''):
        # Extract query parameters from request to forward them to the MPI mediator
        query_params = request.GET.urlencode()
        full_path = extra + ('?' + query_params if query_params else '')
        status_code, result = self.forward_to_mpi_mediator(full_path, method='GET')

        return JsonResponse(result, status=status_code, content_type='application/fhir+json', safe=False)

    def post(self, request):
        now_utc = datetime.datetime.now(pytz.timezone('UTC'))  # Get UTC time
        timestamp = now_utc.strftime("%Y-%m-%d %H:%M:%S %Z") 

        fhir_data = json.loads(request.body)
        fhir_data_original = copy.deepcopy(fhir_data)
        transformed_data = transform_fhir_bundle(fhir_data)

        # Usage within your view or method:
        status_code, result = self.forward_to_mpi_mediator(transformed_data)

        # self.add_orchestration(result, fhir_data_original, transformed_data, timestamp)

        return JsonResponse(result, status=status_code, content_type='application/fhir+json', safe=False)
        
    def forward_to_mpi_mediator(self, fhir_data, method='POST'):
        """
        Forwards data to the MPI mediator endpoint using either GET or POST, and returns the response.

        :param data: The data to be forwarded or the query string for GET requests.
        :param method: The HTTP method to use ('GET' or 'POST').
        :return: A tuple containing a boolean indicating success, and the response data or error message.
        """
        mediator_url = "http://mpi-mediator:3000/fhir"
        headers = {'Content-Type': 'application/fhir+json'}

        try:
            if method == 'POST':
                response = requests.post(mediator_url, json=fhir_data, headers=headers)
                
            elif method == 'GET':
                # Append query string for GET request
                full_url = f"{mediator_url}/{fhir_data}".rstrip('/') if fhir_data else mediator_url
                response = requests.get(full_url, headers=headers)
            else:
                return False, "Unsupported HTTP method"
            
            response_data = response.json()
            # status is incorrectly defined in MPI mediator for success (successful)
            # deleting it will make OpenHIM decide on the status based on the supplied http status code
            del response_data["status"]
            body_content = response_data.get('response', {}).get('body', {})
            if isinstance(body_content, str):
                body_content = json.loads(body_content)
            return response.status_code, body_content
        except requests.exceptions.RequestException as e:
            # Handle connection errors, timeouts, etc.
            return False, f"Request failed: {str(e)}"

    def add_orchestration(self, result, initial_data, updated_data, timestamp):
        # add orchestration data (OpenHIM)
        # TODO: move into class
        orchestration = {
            "name": "OCL Terminology Resolution",
            "request": {
                "headers": {
                    "Content-Type": "application/fhir+json"
                },
                "method": "PATCH",
                "body": json.dumps(initial_data),
                "timestamp": timestamp
            },
            "response": {
                "headers": {
                    "Content-Type": "application/fhir+json"
                },
                "body": json.dumps(updated_data),
                "status": 200 # maybe not needed as not request was sent...
            }
        }
        if isinstance(result, str):
            try:
                result = json.loads(result)
            except json.JSONDecodeError:
                # Handle the case where the string cannot be converted to a dictionary
                # This might involve initializing result to an empty dictionary or handling the error in another way
                result = {}
        if "orchestrations" not in result:
            result["orchestrations"] = []

        result["orchestrations"].append(orchestration)

        return result

class ForwardFHIRPatientDetailsView(APIView):
    permission_classes = [AllowAny]
    def get(self, request, *args, **kwargs):
        # Base URL to which the request will be forwarded
        base_forward_url = 'http://openhim-mapping-mediator:3003'
        
        # Construct the complete forwarding URL by appending the request path
        # Note: request.get_full_path() includes both the path and query parameters
        forward_url = f"{base_forward_url}{request.get_full_path()}"

        # Forward the GET request
        # Note: The request is forwarded with its full path and query parameters
        # Forward the GET request
        response = requests.get(forward_url, headers={'Content-Type': 'application/fhir+json'})

        # Create a Django HttpResponse object with the forwarded response content
        # and set the content_type to 'application/fhir+json'
        django_response = HttpResponse(response.content, content_type='application/fhir+json', status=response.status_code)

        # Return the modified response
        return django_response

class RegisterPatientIDGeneratorView(APIView):
    """
    API View for generating and registering a patient ID.
    """
    def post(self, request) -> Response:
        """
        Handles POST request to generate a unique patient ID and GUID, then registers them.

        :param request: Django Rest Framework request object.
        :return: Response object with the registration result.
        """
        try:
            json_data = request.data
            json_data["patientId"] = str(uuid.uuid4())
            json_data["guid"] = str(uuid.uuid4())
            url = f"http://{openhim_host}:{openhim_port}/register-response"
            payload = json.dumps(json_data)
            headers = {
                'Authorization': 'bearer {{Token}}',
                'Content-Type': 'application/json'
            }

            response = requests.post(url, headers=headers, data=payload)
            return Response(json.loads(response.text))
        except Exception as e:
            return Response({"Error": "An internal server error occurred", "Exception": str(e)}, status=500)

class FindAndRegisterPatientView(APIView):
    """
    View to find and register a patient, updating FHIR data with a golden ID.
    """

    def post(self, request) -> Response:
        """
        Processes a POST request to find or register a patient and update FHIR data with a golden ID.

        :param request: The request object containing FHIR data.
        :return: A Response object with the updated FHIR data or an error message.
        """
        try:
            json_data: Dict[str, Any] = request.data
            parameters: List[Dict[str, str]] = []
            patient_fhir_payload: Dict[str, Any] = {}
            goldenId: str = ""
            status_code: int = 0

            # Extract patient data and identifiers
            for json_data_resource in json_data.get("entry", []):
                resource = json_data_resource.get("resource")
                if resource and resource.get("resourceType") == "Patient":
                    patient_fhir_payload = resource
                    identifiers = resource.get("identifier", [])
                    self.search_patient_body(identifiers, parameters)
                    break

            goldenRecords, status_code = self.get_golden_id_by_interaction(parameters)

            # Process golden records to determine action
            if goldenRecords.get("entry") and goldenRecords.get("total") == 1:
                goldenId = goldenRecords["entry"][0]["resource"]["id"]
                print(f"Patient found for Golden ID: {goldenId}")
            elif goldenRecords.get("total") == 0:
                print("Patient not found. Registering patient in JeMPI.")
                goldenId, status_code = self.register_patient_get_golden_id(patient_fhir_payload)
                print(f"Patient registered and GoldenID is {goldenId}")
            else:
                return Response({"Error": "Invalid FHIR Bundle"}, status=403)

            # Update FHIR data with the new or found golden ID
            if goldenId and status_code in [200, 201]:
                return self.update_and_forward_fhir_data(json_data, goldenId)
            elif status_code == 409:
                return Response({"Error": "Patient already exists with these IDs"}, status=409)
            else:
                return Response({"Error": "Internal Server Error"}, status=503)
        except Exception as e:
            print(e)
            return Response({"Error": "An internal server error occurred", "Exception": str(e)}, status=500)

    def register_patient_get_golden_id(self, patient_fhir_payload):
        """
        Registers a patient and retrieves the golden ID.

        :param patient_fhir_payload: The FHIR data payload for the patient.
        :return: A tuple containing the golden ID and the status code.
        """
        registerUrl = f"http://openhim-core:5001/fhir/Patient"
        headers = {'Content-Type': 'application/json'}
        registerPayload = json.dumps(patient_fhir_payload)
        response = requests.post(registerUrl, headers=headers, data=registerPayload)

        if response.status_code == 201:
            newGoldenRecords = response.json()
            goldenId = newGoldenRecords.get("id", "")
            return goldenId, 201
        else:
            print(response.text)
            return response.text, response.status_code

    def get_golden_id_by_interaction(self, parameters):
        """
        Searches for a patient using identifiers to retrieve the golden ID.

        :param parameters: A list of patient identifiers.
        :return: A tuple containing the search results and the status code.
        """
        searchUrl = f"http://openhim-core:5001/fhir/Patients"
        headers = {'Content-Type': 'application/json'}
        payload = json.dumps({"resourceType": "Parameters", "parameter": parameters})
        response = requests.post(searchUrl, headers=headers, data=payload)

        if response.status_code == 200:
            return response.json(), 200
        else:
            print(response.text)
            return {}, response.status_code

    def search_patient_body(self, identifiers, parameters) -> None:
        """
        Prepares search parameters from patient identifiers.

        :param identifiers: A list of patient identifiers.
        :param parameters: A list to which search parameters will be appended.
        """
        for identifier in identifiers:
            system = identifier.get("system", "").lower()
            if system in ["phn", "nic", "ppn", "scn", "dl"] and identifier.get("value"):
                parameters.append({
                    "name": "and",
                    "valueCode": system,
                    "valueString": identifier.get("value")
                })

    def update_and_forward_fhir_data(self, json_data: Dict[str, Any], goldenId: str) -> Response:
        """
        Updates FHIR data with the golden ID and forwards it to a specified endpoint.

        :param json_data: The original FHIR data.
        :param goldenId: The golden ID to be applied to the FHIR data.
        :return: A Response object with the forwarding result.
        """
        new_fhir_json = []
        for fhir_json_data in json_data.get("entry"):
            fhir_json_resource = fhir_json_data.get("resource")
            if fhir_json_resource:
                if fhir_json_resource.get("resourceType") == "Patient":
                    print("Recreating patient resource for FHIR with Golden ID")
                    patient = {
                        "fullUrl": "Patient/"+goldenId,
                        "resource": {
                            "resourceType": "Patient",
                            "id": goldenId,
                        },
                        "request": {
                            "method": "PUT",
                            "url": "Patient/"+goldenId
                        }
                    }
                    new_fhir_json.append(patient)
                if fhir_json_resource.get("resourceType") != "Patient":
                    fhirSubject = fhir_json_resource.get("subject")
                    fhirPatient = fhir_json_resource.get("patient")
                    fhirReceiver = fhir_json_resource.get("receiver")
                    if fhirSubject:
                        fhir_json_data["resource"]["subject"]["reference"] ="Patient/"+goldenId
                    if fhirPatient:
                        fhir_json_data["resource"]["patient"]["reference"] ="Patient/"+goldenId
                    if fhirReceiver:
                        fhir_json_data["resource"]["receiver"][0]["reference"] = "Patient/" + goldenId
                    new_fhir_json.append(fhir_json_data)

        json_data["entry"] = new_fhir_json
        if json_data.get("entry"):
            url = "http://openhim-core:5001/fhir"
            payload = json.dumps(json_data)
            headers = {
                'Content-Type': 'application/json'
            }

            response = requests.request("POST", url, headers=headers, data=payload)
            return Response(json.loads(response.text))