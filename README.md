# ips-on-platform
This repo contains the required config to start up a Hapi FHIR server alongside the OpenHIM and a reverse proxy to be used for IPS Platform purposes.

## Prerequisites:
1. Docker
1. An Active Docker Swarm

## Getting Started:
1. Check that you have an active docker swarm running on the respective environment. `docker info | grep Swarm`
1. If no swarm is running, you can start a swarm with `docker swarm init`.
1. Ensure that you have the correct IG URL specified in the respective environment variable file (e.g. `.env.local`). The variable to set is `FHIR_IG_URL`.
1. Run `./get-cli.sh` to download the latest release of the CLI. You can download only the specific CLI for your operating system by providing it as a parameter. (e.g. `./get-cli.sh linux`)
1. Run the relevant deploy script. (e.g. `./instant-linux package init -p local`)

### Testing it out

To test out the services are all working as intended to process the FHIR bundle, we can send through a few example bundles. A publicly available Postman collection can be found here: https://www.postman.com/jembi-platform/workspace/techhub-ips-on-platform/overview

You can also run a local smoke test to verify the workflows are running as intended. Follow the steps defined within the `performance` folder for running the smoke test: https://gitlab.com/healthtechhub/ips-on-platform-v2/-/blob/main/performance/README.md#smoke-test
